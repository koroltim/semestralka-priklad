# Program for calculating shortest path in graphs
This program solves problem of calculating shortest paths in graphs.
Original task is:
Nejkratší cesty mezi všemi vrcholy (shortest paths)
Další z velmi dobře prozkoumaných grafových algoritmů, které mají využití jak ve fyzickém světě, tak ve světě počítačů. Na výstupu je seznam dvojic vrcholů s délkou minimální cesty mezi nimi (pokud existuje) a vypsanou cestou.

## Implementation
Implementation in this program is the dumbest possible.
Algorithm is simple brute force.
After inserting nodes,edges and starting the calculation process it simply creates all paths available.
After that it picks the shortest paths between each pair of nodes available.
Nodes which are not connected to the main graph are possible. 
They will still be calculated , so it's possible to calculate paths in different graphs in one run , but expect some paths missing as program calculates paths between ALL nodes.

Running

If you want to use interactive version of program you should just boot it without arguments.
1)At first you should enter the nodes you want to work with.
Using 'Node nodeName' command where nodeName is the name of the node you want to create.
2)After that you should enter edges between the existing nodes.
Using 'Edge nameFrom nameTo cost' command where nameFrom is the name of the node where the edge starts , nameTo is the name of the node where the edge ends and cost is distance between nodes , has to be integer .
3)After all nodes and edges are inserted into the program, you can calculate the paths.
Use 'Calculate' command to calculate the shortest paths between all of the nodes.
4)After that you can insert new nodes and edges and calculate paths as many times as you want.
5)To finish the program type 'End' command. 

If you want to use interactive version of program you should
Print the same commands as mentioned above each one at its own line.
Then you should boot the program with '-f textFileName' argument where textFileName is the name of the file where input is stored.
Output will still be printed out as standard output.

## Measuring
Irrelative since program has implementation for 1 thread only.