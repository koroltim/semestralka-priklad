#include <iostream>
#include <vector>
#include <algorithm>
#include <string>
#include <cstring>
#include "app.cpp"
#include "sstream"
#include <fstream>

void print_usage(std::string const& exe_name) {
    std::clog << "Usage: " << exe_name << " -f NameOfInputFile";
}

void print_help(){
    std::cout << "List of commands:" << '\n';
    std::cout << "Type 'Node nodeName' where nodeName is the name of the node you want to create." << '\n';
    std::cout << "Type 'Edge nameFrom nameTo cost' where nameFrom is the name of the node where the edge starts , nameTo is the name of the node where the edge ends and cost is distance between nodes , has to be integer ." << '\n';
    std::cout << "Type 'Calculate' to calculate shortest paths between all of the nodes." << '\n';
    std::cout << "Type 'End' to finish the program." << '\n';
    std::cout << "Alternatively you can type commands into text file and start the program with arguments '-f textFileName' where textFileName is the name of the file where input is stored." << '\n';
}

bool is_help(std::string const& argument) {
    return argument == "--help" || argument == "-h";
}

bool isDigit(std::string& input){
    bool isNumber = true;
    for(char x : input){
        if(!isdigit(x)){
            isNumber = false;
        }
    }
    return isNumber;
}

std::vector<std::string> split(std::string& x){
    char delim = ' ';
    x += delim; //includes a delimiter at the end so last word is also read
    std::vector<std::string> splitted;
    std::string temp;
    for (int i = 0; i < x.length(); i++)
    {
        if (x[i] == delim)
        {
            splitted.push_back(temp); //store words in "splitted" vector
            temp = "";
            i++;
        }
        temp += x[i];
    }
    return splitted;
}

int main(int argc, char** argv) {
    std::string file;

    if (std::any_of(argv, argv+argc, is_help)) {
        print_usage(argv[0]);
        return 0;
    }
    
    if (argc == 1) {
        std::clog << "No arguments given,reading from standard input.\n";
        std::string line;
        auto graph = Graph();
        while (std::getline(std::cin, line)) {
            std::vector<std::string> commands = split(line);
            if(commands.size() == 1){
                if(is_help(commands[0])){
                    print_help();
                } else if(commands[0] == "End"){
                    std::cout << "End command received, finishing the program."<< '\n';
                    return 0;
                } else if(commands[0] == "Calculate"){
                    std::cout << "Calculating shortest path for the graph" << '\n';
                    graph.calculateAllPaths();
                } else {
                    std::cout << "Invalid command given, ignoring. Type --help or -h for help." << '\n';
                }
            } else if(commands.size() == 2){
                if(commands[0] == "Node"){
                    graph.addNode(commands[1]);
                } else {
                    std::cout << "Invalid command given, ignoring. Type --help or -h for help." << '\n';
                }
            } else if(commands.size() == 4){
                if(commands[0] == "Edge"){
                    if(isDigit(commands[3])){
                        std::stringstream intValue(commands[3]);
                        int cost = 0;
                        intValue >> cost;
                        graph.addEdge(commands[1], commands[2], cost);
                    } else {
                        std::cout << "Fourth argument is supposed to be number, ignoring. Type --help or -h for help." << '\n';
                    }
                } else {
                    std::cout << "Invalid command given, ignoring. Type --help or -h for help." << '\n';
                }
            } else if(commands.size() > 4){
                std::cout << "Too many arguments given" << '\n';
            } else if(commands.size() == 3){
                std::cout << "Invalid command given, ignoring. Type --help or -h for help." << '\n';
            }
        }
        return 0;
    } else if (argc >= 4) {
        std::clog << "Too many arguments given.\n";
        print_usage(argv[0]);
        return 1;
    } else if(argc == 2){
        std::clog << "Too few arguments given,require 2 arguments, finishing program.\n";
        return 1;
    } else {
        if (strcmp(argv[1], "-f") == 0) {
            std::string line;
            file = argv[2];
            std::ifstream MyReadFile(file);
            if (!file.empty()) {
                if(!MyReadFile.is_open()){
                    std::clog << "Error opening file "<< file << "!\nTerminating the program!!!\n";
                    exit (EXIT_FAILURE);
                }
            }
            std::clog << "File " << file << " is used as input.\n";
            auto graph = Graph();
            while (getline (MyReadFile, line)) {
                std::vector<std::string> commands = split(line);
                if(commands.size() == 1){
                    if(is_help(commands[0])){
                        print_help();
                    } else if(commands[0] == "End"){
                        std::cout << "End command received, finishing the program."<< '\n';
                        return 0;
                    } else if(commands[0] == "Calculate"){
                        std::cout << "Calculating shortest path for the graph" << '\n';
                        graph.calculateAllPaths();
                    } else {
                        std::cout << "Invalid command given, ignoring. Type --help or -h for help." << '\n';
                    }
                } else if(commands.size() == 2){
                    if(commands[0] == "Node"){
                        graph.addNode(commands[1]);
                    } else {
                        std::cout << "Invalid command given, ignoring. Type --help or -h for help." << '\n';
                    }
                } else if(commands.size() == 4){
                    if(commands[0] == "Edge"){
                        if(isDigit(commands[3])){
                            std::stringstream intValue(commands[3]);
                            int cost = 0;
                            intValue >> cost;
                            graph.addEdge(commands[1], commands[2], cost);
                        } else {
                            std::cout << "Fourth argument is supposed to be number, ignoring. Type --help or -h for help." << '\n';
                        }
                    } else {
                        std::cout << "Invalid command given, ignoring. Type --help or -h for help." << '\n';
                    }
                } else if(commands.size() > 4){
                    std::cout << "Too many arguments given" << '\n';
                } else if(commands.size() == 3){
                    std::cout << "Invalid command given, ignoring. Type --help or -h for help." << '\n';
                }
            }
            MyReadFile.close();
            return 0;
        } else {
            std::clog << "Unknown command parsed as first argument.\n";
            return 1;
        }
    }

}