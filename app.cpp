//
// Created by Strel on 15.01.2022.
//
#include <vector>
#include <iostream>
#include <string>
class Edge{
    std::vector<std::string> nodes;
    int cost;
public:
    void setCost(int& costed){
        this->cost = costed;
    }
    int getCost() const{
        return this->cost;
    }
    void addNode(std::string& node){
        this->nodes.push_back(node);
    }
    std::vector<std::string> getNodes(){
        return this->nodes;
    }
};

class Path{
    std::vector<std::string> path;
    int cost;
public:
    void setCost(int& costed){
        this->cost = costed;
    }
    int getCost() const{
        return cost;
    }
    void addNode(std::string& node){
        this->path.push_back(node);
    }
    void increaseCost(int& costed){
        this->cost = this->cost + costed;
    }
    std::vector<std::string> getPath(){
        return this->path;
    }
    void printOutPath(){
        std::cout << "Shortest path from " << path[0] << " to " << path.back() << " is "  ;
        for(int i = 0; i < path.size() - 1; i++){
            std::cout << path[i] << "-" ;
        }
        std::cout << path.back() << " with cost of " << this->cost << '\n' ;
    }
};

class Graph{
    std::vector<std::string> nodes;
    std::vector<Edge> edges;
    std::vector<Path> paths;
    std::vector<Path> candidates;
public:
    void addNode(std::string& nodeName){
        if(std::count(nodes.begin(), nodes.end() , nodeName)){
            std::cout << "Node with name " << nodeName << " is already in graph , skipping this command.\n";
        }else{
            std::cout << "Node with name " << nodeName << " has been created!\n";
            this->nodes.push_back(nodeName);
        }
    }
    void addEdge(std::string& from , std::string& to , int& cost){
        if(from != to) {
            Edge newEdge = Edge();
            bool edgeAlreadyExists = false;
            bool allNodesExist = true;
            if (!std::count(nodes.begin(), nodes.end(), from)) {
                allNodesExist = false;
                std::cout << "Node with name " << from << " doesn't exist, skipping this command.\n";
            }
            if (!std::count(nodes.begin(), nodes.end(), to)) {
                allNodesExist = false;
                std::cout << "Node with name " << to << " doesn't exist, skipping this command.\n";
            }
            for (Edge edge: edges) {
                std::vector<std::string> edgeNodes = edge.getNodes();
                if (std::count(edgeNodes.begin(), edgeNodes.end(), from) &&
                    std::count(edgeNodes.begin(), edgeNodes.end(), to)) {
                    edgeAlreadyExists = true;
                }
            }
            if (allNodesExist) {
                if (!edgeAlreadyExists) {
                    newEdge.addNode(from);
                    newEdge.addNode(to);
                    newEdge.setCost(cost);
                    edges.push_back(newEdge);
                    std::cout << "Edge from " << from << " to " << to << " with cost of " << cost
                              << " has been created!\n";
                } else {
                    std::cout << "Edge from " << from << " to " << to << " already exists!\n";
                }
            }
        } else {
            std::cout << "Cannot create edge from " << from << " to " << to << " as it's the same node!\n";
        }


    }

    void bruteForceAPath(const std::vector<std::string>& exploredNodes , const Path& path){
        for(auto & edge : edges){
            std::vector<std::string> explored = exploredNodes;
            Path currentPath = path;
            std::vector<std::string> edgesNodes = edge.getNodes();
            std::vector<std::string> pathNodes = currentPath.getPath();
            std::string leftNode = edge.getNodes()[0];
            std::string rightNode = edge.getNodes()[1];
            if(std::count(edgesNodes.begin(), edgesNodes.end(), leftNode) && pathNodes.back() == leftNode){
                if(!std::count(explored.begin(), explored.end(), rightNode)){
                    int cost = edge.getCost();
                    currentPath.increaseCost(cost);
                    currentPath.addNode(rightNode);
                    explored.push_back(rightNode);
                    candidates.push_back(currentPath);
                    if(currentPath.getPath().size() != nodes.size()){
                        bruteForceAPath(explored , currentPath);
                    }
                }
            } else if(std::count(edgesNodes.begin(), edgesNodes.end(), rightNode) && pathNodes.back() == rightNode){
                if(!std::count(explored.begin(), explored.end(), leftNode)){
                    int cost = edge.getCost();
                    currentPath.increaseCost(cost);
                    currentPath.addNode(leftNode);
                    explored.push_back(leftNode);
                    candidates.push_back(currentPath);
                    if(currentPath.getPath().size() != nodes.size()){
                        bruteForceAPath(explored , currentPath);
                    }
                }
            }
        }
    }

    void bruteForceAllPaths(){
        for(auto & edge : edges){
            std::vector<std::string> explored;
            Path currentPath = Path();
            int cost = 0;
            currentPath.setCost(cost);
            std::string node = edge.getNodes()[0];
            currentPath.addNode(node);
            explored.push_back(node);
            bruteForceAPath(explored , currentPath);
        }

    }

    void pathFor(std::string& from , std::string& to){
        std::vector<Path> participants;
        for(Path pef : candidates){
            if(pef.getPath()[0] == from && pef.getPath().back() == to || pef.getPath()[0] == to && pef.getPath().back() == from){
                participants.push_back(pef);
            }
        }
        if(!participants.empty()) {
            Path pathWithLowestCost = participants[0];
            for (auto & participant : participants) {
                if (participant.getCost() < pathWithLowestCost.getCost()) {
                    pathWithLowestCost = participant;
                }
            }
            paths.push_back(pathWithLowestCost);
        } else {
            std::cout << "No path from "<< from << " to " << to << " has been found!\n" ;
        }
    }

    void calculateAllPaths(){
        bruteForceAllPaths();
        for(int i = 0 ; i < nodes.size(); i++){
            for (int j = i + 1; j < nodes.size(); ++j) {
                //find required paths
                pathFor(nodes[i] , nodes[j]);
            }
        }
        for(Path pef : paths){
            pef.printOutPath();
        }
        paths.clear();
        std::cout << "Paths have been cleared!" << '\n';
    }

};

